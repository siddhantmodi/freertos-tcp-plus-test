/*
 * FreeRTOS Kernel V10.3.0
 * Copyright (C) 2020 MistyWest, Inc. or its affiliates.  All Rights Reserved.
 *
 * 1 tab == 4 spaces!
 */

#ifndef TCP_SEND_RECEIVE_DATA_H
#define TCP_SEND_RECEIVE_DATA_H

#define TCP_CONFIG_SERVER_PORT_NUM ( 12345 )

/*
 * Create the public TCP send/receive tasks.
 */

Socket_t vCreateTCPClientSocket( void );
Socket_t vCreateTCPServerSocket( uint16_t pPortNum );

#endif /* TCP_SEND_RECEIVE_DATA_H */


