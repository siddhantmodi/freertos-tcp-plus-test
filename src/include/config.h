/*
 * FreeRTOS Kernel V10.3.0
 * Copyright (C) 2020 MistyWest, Inc. or its affiliates.  All Rights Reserved.
 *
 * 1 tab == 4 spaces!
 */

#ifndef CONFIG_H
#define CONFIG_H

/*
 * Create the public TCP send/receive tasks.
 */

typedef struct configInfo
{
	uint32_t serverAddr0;
	uint32_t serverAddr1;
	uint32_t serverAddr2;
	uint32_t serverAddr3;
	uint32_t portNum;
} configInfo_t;

//typedef bool configResult_t;

/*
 * This task will create and connect a socket to a server, the details
 * for which will be provided in the parameters. Once configuration is
 * complete, this task will notify the surface box regarding the success/
 * failure of the operation.
 */
//void sendConfigDataPacket( configInfo_t pConfigInfo );

void vStartConfigServerTask( uint16_t usStackSize, UBaseType_t uxPriority );

#endif /* CONFIG_H */
