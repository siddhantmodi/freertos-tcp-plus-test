/*
 * FreeRTOS Kernel V10.3.0
 * Copyright (C) 2020 MistyWest, Inc. or its affiliates.  All Rights Reserved.
 *
 * 1 tab == 4 spaces!
 */

/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/* FreeRTOS+TCP includes. */
#include "FreeRTOS_IP.h"
#include "FreeRTOS_Sockets.h"

#include "TCPSendReceiveData.h"

#define TCP_SERVER_PORT_NUMBER	( 15691 )

Socket_t vCreateTCPServerSocket( uint16_t pPortNum )
{
	struct freertos_sockaddr xBindAddress;
	Socket_t xListeningSocket;
	static const TickType_t xReceiveTimeOut = portMAX_DELAY;

#if( ipconfigUSE_TCP_WIN == 1 )
	WinProperties_t xWinProps;

	/* Fill in the buffer and window sizes that will be used by the socket. */
	xWinProps.lTxBufSize = ipconfigTCP_TX_BUFFER_LENGTH;
	xWinProps.lTxWinSize = configECHO_SERVER_TX_WINDOW_SIZE;
	xWinProps.lRxBufSize = ipconfigTCP_RX_BUFFER_LENGTH;
	xWinProps.lRxWinSize = configECHO_SERVER_RX_WINDOW_SIZE;
#endif /* ipconfigUSE_TCP_WIN */

    /* Attempt to open the socket. */
    xListeningSocket = FreeRTOS_socket( FREERTOS_AF_INET, FREERTOS_SOCK_STREAM, FREERTOS_IPPROTO_TCP );

    /* Check the socket was created. */
    configASSERT( xListeningSocket != FREERTOS_INVALID_SOCKET );

    /* If FREERTOS_SO_RCVBUF or FREERTOS_SO_SNDBUF are to be used with
    FreeRTOS_setsockopt() to change the buffer sizes from their default then do
    it here!.  (see the FreeRTOS_setsockopt() documentation. */

    /* If ipconfigUSE_TCP_WIN is set to 1 and FREERTOS_SO_WIN_PROPERTIES is to
    be used with FreeRTOS_setsockopt() to change the sliding window size from
    its default then do it here! (see the FreeRTOS_setsockopt()
    documentation. */

    /* Set a time out so accept() will just wait for a connection. */
	FreeRTOS_setsockopt( xListeningSocket, 0, FREERTOS_SO_RCVTIMEO, &xReceiveTimeOut, sizeof( xReceiveTimeOut ) );

#if( ipconfigUSE_TCP_WIN == 1 )
    /* Set the window and buffer sizes. */
    FreeRTOS_setsockopt( xListeningSocket, 0, FREERTOS_SO_WIN_PROPERTIES, ( void * ) &xWinProps, sizeof( xWinProps ) );
#endif /* ipconfigUSE_TCP_WIN */

    /* Set the listening port to pPortNum. */
    xBindAddress.sin_port = FreeRTOS_htons( pPortNum ); // _htons is a endianness related function

    /* Bind the socket to the port that the client RTOS task will send to. */
    FreeRTOS_bind( xListeningSocket, &xBindAddress, sizeof( xBindAddress ) );

    return xListeningSocket;
}

Socket_t vCreateTCPClientSocket( void )
{
	Socket_t xClientSocket;
	socklen_t xSize = sizeof( struct freertos_sockaddr );
	static const TickType_t xTimeOut = pdMS_TO_TICKS( 2000 );

    /* Attempt to open the socket. */
    xClientSocket = FreeRTOS_socket( FREERTOS_AF_INET, FREERTOS_SOCK_STREAM, FREERTOS_IPPROTO_TCP );

    /* Check the socket was created. */
    configASSERT( xClientSocket != FREERTOS_INVALID_SOCKET );

    /* If FREERTOS_SO_RCVBUF or FREERTOS_SO_SNDBUF are to be used with
    FreeRTOS_setsockopt() to change the buffer sizes from their default then do
    it here!.  (see the FreeRTOS_setsockopt() documentation. */

    /* If ipconfigUSE_TCP_WIN is set to 1 and FREERTOS_SO_WIN_PROPERTIES is to
    be used with FreeRTOS_setsockopt() to change the sliding window size from
    its default then do it here! (see the FreeRTOS_setsockopt()
    documentation. */

    /* Set send and receive time outs. */
    FreeRTOS_setsockopt( xClientSocket,
                         0,
                         FREERTOS_SO_RCVTIMEO,
                         &xTimeOut,
                         sizeof( xTimeOut ) );

    FreeRTOS_setsockopt( xClientSocket,
                         0,
                         FREERTOS_SO_SNDTIMEO,
                         &xTimeOut,
                         sizeof( xTimeOut ) );

    /* Bind the socket, but pass in NULL to let FreeRTOS+TCP choose the port number.*/
    FreeRTOS_bind( xClientSocket, NULL, xSize );

    return xClientSocket;
}

//static void vCreateTCPServerSocket( void )
//{
//	struct freertos_sockaddr xClient, xBindAddress;
//	Socket_t xListeningSocket, xConnectedSocket;
//	socklen_t xSize = sizeof( xClient );
//	static const TickType_t xReceiveTimeOut = portMAX_DELAY;
//	const BaseType_t xBacklog = 20;
//
//    /* Attempt to open the socket. */
//    xListeningSocket = FreeRTOS_socket( PF_INET,
//                                        SOCK_STREAM,  /* SOCK_STREAM for TCP. */
//                                        IPPROTO_TCP );
//
//    /* Check the socket was created. */
//    configASSERT( xListeningSocket != FREERTOS_INVALID_SOCKET );
//
//    /* If FREERTOS_SO_RCVBUF or FREERTOS_SO_SNDBUF are to be used with
//    FreeRTOS_setsockopt() to change the buffer sizes from their default then do
//    it here!.  (see the FreeRTOS_setsockopt() documentation. */
//
//    /* If ipconfigUSE_TCP_WIN is set to 1 and FREERTOS_SO_WIN_PROPERTIES is to
//    be used with FreeRTOS_setsockopt() to change the sliding window size from
//    its default then do it here! (see the FreeRTOS_setsockopt()
//    documentation. */
//
//    /* Set a time out so accept() will just wait for a connection. */
//    FreeRTOS_setsockopt( xListeningSocket,
//                         0,
//                         FREERTOS_SO_RCVTIMEO,
//                         &xReceiveTimeOut,
//                         sizeof( xReceiveTimeOut ) );
//
//    /* Set the listening port to 10000. */
//    xBindAddress.sin_port = ( uint16_t ) 10000;
//    xBindAddress.sin_port = FreeRTOS_htons( xBindAddress.sin_port );
//
//    /* Bind the socket to the port that the client RTOS task will send to. */
//    FreeRTOS_bind( xListeningSocket, &xBindAddress, sizeof( xBindAddress ) );
//
//    /* Set the socket into a listening state so it can accept connections.
//    The maximum number of simultaneous connections is limited to 20. */
//    FreeRTOS_listen( xListeningSocket, xBacklog );
//
//    for( ;; )
//    {
//        /* Wait for incoming connections. */
//        xConnectedSocket = FreeRTOS_accept( xListeningSocket, &xClient, &xSize );
//        configASSERT( xConnectedSocket != FREERTOS_INVALID_SOCKET );
//
//        /* Spawn a RTOS task to handle the connection. */
//        xTaskCreate( prvServerConnectionInstance,
//                     “EchoServer”,
//                     usUsedStackSize,
//                     ( void * ) xConnectedSocket,
//                     tskIDLE_PRIORITY,
//                     NULL );
//    }
//}

