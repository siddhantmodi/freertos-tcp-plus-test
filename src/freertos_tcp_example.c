/*
    Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
    Copyright (c) 2012 - 2020 Xilinx, Inc. All Rights Reserved.
	SPDX-License-Identifier: MIT


    http://www.FreeRTOS.org
    http://aws.amazon.com/freertos


    1 tab == 4 spaces!
*/

#include <stdlib.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Xilinx includes. */
#include "xil_printf.h"
#include "xparameters.h"

/* AXI GPIO Driver */
#include "xgpio.h"

/* FreeRTOS+TCP includes. */
#include "FreeRTOS_IP.h"
#include "FreeRTOS_Sockets.h"
//#include "FreeRTOS_tcp_server.h"
#include "FreeRTOS_DHCP.h"

/* TCP Echo Task file */
#include "TCPEchoClient_SingleTasks.h"

/* Simple echo task parameters ---------------------------------------*/
#define mainECHO_CLIENT_TASK_PRIORITY		( tskIDLE_PRIORITY + 1 )
#define mainECHO_CLIENT_TASK_STACK_SIZE 	( configMINIMAL_STACK_SIZE * 3 )
#define mainCONFIG_SERVER_TASK_PRIORITY     (tskIDLE_PRIORITY + 1)
#define mainCONFIG_SERVER_TASK_STACK_SIZE 	( configMINIMAL_STACK_SIZE * 3 ) // TODO: Set optimal stack size, priority, etc...
#define mainHOST_NAME			"RTOSDemo"
#define mainDEVICE_NICK_NAME	"Zynq"
/*-----------------------------------------------------------*/

/*Default MAC address configuration. */
const uint8_t ucMACAddress[ 6 ] = { configMAC_ADDR0, configMAC_ADDR1, configMAC_ADDR2, configMAC_ADDR3, configMAC_ADDR4, configMAC_ADDR5 };

/*
 * Define the network addressing.  These parameters will be used if either
 * ipconfigUDE_DHCP is 0 or if ipconfigUSE_DHCP is 1 but DHCP auto configuration
 * failed.
 */
static const uint8_t ucIPAddress[ 4 ] = { configIP_ADDR0, configIP_ADDR1, configIP_ADDR2, configIP_ADDR3 };
static const uint8_t ucNetMask[ 4 ] = { configNET_MASK0, configNET_MASK1, configNET_MASK2, configNET_MASK3 };
static const uint8_t ucGatewayAddress[ 4 ] = { configGATEWAY_ADDR0, configGATEWAY_ADDR1, configGATEWAY_ADDR2, configGATEWAY_ADDR3 };

/* The following is the address of an OpenDNS server. */
static const uint8_t ucDNSServerAddress[ 4 ] = { configDNS_SERVER_ADDR0, configDNS_SERVER_ADDR1, configDNS_SERVER_ADDR2, configDNS_SERVER_ADDR3 };

/* Use by the pseudo random number generator.
 * TODO: Remove array when done with initial tests and new RNG is in place.*/
//static const UBaseType_t ulNextRand[] = {1243, 92834, 7285721, 8844771, 231};
/*-----------------------------------------------------------*/

int main( void )
{
	BaseType_t status;

	xil_printf( "Hello from FreeRTOS TCP example main\r\n" );

	/* Initialise the RTOS�s TCP/IP stack.  The tasks that use the network
	   are created in the vApplicationIPNetworkEventHook() hook function
	   below.  The hook function is called when the network connects. */
	status = FreeRTOS_IPInit(ucIPAddress,
	                		 ucNetMask,
	                         ucGatewayAddress,
	                         ucDNSServerAddress,
							 ucMACAddress);
	configASSERT(status==pdPASS);

	 /* Other RTOS tasks can be created here. */

	/* Start the tasks and timer running. */
	vTaskStartScheduler();

	/* If all is well, the scheduler will now be running, and the following line
	will never be reached.  If the following line does execute, then there was
	insufficient FreeRTOS heap memory available for the idle and/or timer tasks
	to be created.  See the memory management section on the FreeRTOS web site
	for more details. */
	for( ;; );
}
/*-----------------------------------------------------------*/

/* Called by FreeRTOS+TCP when the network connects or disconnects.  Disconnect
events are only received if implemented in the MAC driver. */
void vApplicationIPNetworkEventHook( eIPCallbackEvent_t eNetworkEvent )
{
    uint32_t ulIPAddress, ulNetMask, ulGatewayAddress, ulDNSServerAddress;
    char cBuffer[ 16 ];
    static BaseType_t xTasksAlreadyCreated = pdFALSE;

	/* If the network has just come up...*/
	if( eNetworkEvent == eNetworkUp )
	{
		/* Create the tasks that use the IP stack if they have not already been
		created. */
		if( xTasksAlreadyCreated == pdFALSE )
		{
//			vStartTCPEchoClientTasks_SingleTasks(mainECHO_CLIENT_TASK_STACK_SIZE, mainECHO_CLIENT_TASK_PRIORITY);
//			vStartTCPSocketCreate(mainECHO_CLIENT_TASK_STACK_SIZE, mainECHO_CLIENT_TASK_PRIORITY);
			vStartConfigServerTask(mainCONFIG_SERVER_TASK_STACK_SIZE, mainCONFIG_SERVER_TASK_PRIORITY);
			xTasksAlreadyCreated = pdTRUE;
		}

		/* Print out the network configuration, which may have come from a DHCP
		server. */
		FreeRTOS_GetAddressConfiguration( &ulIPAddress, &ulNetMask, &ulGatewayAddress, &ulDNSServerAddress );
		FreeRTOS_inet_ntoa( ulIPAddress, cBuffer );
		xil_printf("IP Address: %s\r\n", cBuffer);

		FreeRTOS_inet_ntoa( ulNetMask, cBuffer );
		xil_printf("Subnet Mask: %s\r\n", cBuffer);

		FreeRTOS_inet_ntoa( ulGatewayAddress, cBuffer );
		xil_printf("Gateway Address: %s\r\n", cBuffer);

		FreeRTOS_inet_ntoa( ulDNSServerAddress, cBuffer );
		xil_printf("DNS Server Address: %s\r\n\r\n\r\n", cBuffer);
	}
}
/*-----------------------------------------------------------*/

void vAssertCalled( const char *pcFile, uint32_t ulLine )
{
volatile uint32_t ulBlockVariable = 0UL;
volatile char *pcFileName = ( volatile char *  ) pcFile;
volatile uint32_t ulLineNumber = ulLine;

	( void ) pcFileName;
	( void ) ulLineNumber;

	FreeRTOS_debug_printf( ( "vAssertCalled( %s, %ld\n", pcFile, ulLine ) );

	/* Setting ulBlockVariable to a non-zero value in the debugger will allow
	this function to be exited. */
	taskDISABLE_INTERRUPTS();
	{
		while( ulBlockVariable == 0UL )
		{
			__asm volatile( "NOP" );
		}
	}
	taskENABLE_INTERRUPTS();
}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */
	vAssertCalled( __FILE__, __LINE__ );
}

/*-----------------------------------------------------------*/

const char *pcApplicationHostnameHook( void )
{
	/* Assign the name "FreeRTOS" to this network node.  This function will be
	called during the DHCP: the machine will be registered with an IP address
	plus this name. */
	return mainHOST_NAME;
}
/*-----------------------------------------------------------*/

BaseType_t xApplicationDNSQueryHook( const char *pcName )
{
BaseType_t xReturn;

	/* Determine if a name lookup is for this node.  Two names are given
	to this node: that returned by pcApplicationHostnameHook() and that set
	by mainDEVICE_NICK_NAME. */
	if( strcasecmp( pcName, pcApplicationHostnameHook() ) == 0 )
	{
		xReturn = pdPASS;
	}
	else if( strcasecmp( pcName, mainDEVICE_NICK_NAME ) == 0 )
	{
		xReturn = pdPASS;
	}
	else
	{
		xReturn = pdFAIL;
	}

	return xReturn;
}
/*-----------------------------------------------------------*/

//eDHCPCallbackAnswer_t xApplicationDHCPHook( eDHCPCallbackPhase_t eDHCPPhase,
//                                            uint32_t ulIPAddress )
//{
//eDHCPCallbackAnswer_t eReturn;
//uint32_t ulStaticIPAddress, ulStaticNetMask;
//
//  /* This hook is called in a couple of places during the DHCP process, as
//  identified by the eDHCPPhase parameter. */
//  switch( eDHCPPhase )
//  {
//    case eDHCPPhasePreDiscover  :
//      /* A DHCP discovery is about to be sent out.  eDHCPContinue is
//      returned to allow the discovery to go out.
//
//      If eDHCPUseDefaults had been returned instead then the DHCP process
//      would be stopped and the statically configured IP address would be
//      used.
//
//      If eDHCPStopNoChanges had been returned instead then the DHCP
//      process would be stopped and whatever the current network
//      configuration was would continue to be used. */
//      eReturn = eDHCPContinue;
//      break;
//
//    case eDHCPPhasePreRequest  :
//      /* An offer has been received from the DHCP server, and the offered
//      IP address is passed in the ulIPAddress parameter.  Convert the
//      offered and statically allocated IP addresses to 32-bit values. */
//      ulStaticIPAddress = FreeRTOS_inet_addr_quick( configIP_ADDR0,
//                                                    configIP_ADDR1,
//                                                    configIP_ADDR2,
//                                                    configIP_ADDR3 );
//
//      ulStaticNetMask = FreeRTOS_inet_addr_quick( configNET_MASK0,
//                                                  configNET_MASK1,
//                                                  configNET_MASK2,
//                                                  configNET_MASK3 );
//
//      /* Mask the IP addresses to leave just the sub-domain octets. */
//      ulStaticIPAddress &= ulStaticNetMask;
//      ulIPAddress &= ulStaticNetMask;
//
//      /* Are the sub-domains the same? */
//      if( ulStaticIPAddress == ulIPAddress )
//      {
//        /* The sub-domains match, so the default IP address can be
//        used.  The DHCP process is stopped at this point. */
//        eReturn = eDHCPUseDefaults;
//      }
//      else
//      {
//        /* The sub-domains don�t match, so continue with the DHCP
//        process so the offered IP address is used. */
//        eReturn = eDHCPContinue;
//      }
//
//      break;
//
//    default :
//      /* Cannot be reached, but set eReturn to prevent compiler warnings
//      where compilers are disposed to generating one. */
//      eReturn = eDHCPContinue;
//      break;
//  }
//
//  return eReturn;
//}
/*-----------------------------------------------------------*/

/* TODO: Create a method to generate a random number specific to the Cortex A9. */
/*-----------------------------------------------------------*/

UBaseType_t uxRand( void )
{
	/* Utility function to generate a pseudo random number. */
//	static uint8_t rand_idx = 0;
//	const uint32_t ulMultiplier = 0x015a4e35UL, ulIncrement = 1UL;
//	uint32_t prv_ulNextRand = ulNextRand[rand_idx];
//
//	prv_ulNextRand = ( ulMultiplier * prv_ulNextRand ) + ulIncrement;
//	rand_idx++;
//	rand_idx = rand_idx % 5;
//
//	return( ( int ) ( prv_ulNextRand >> 16UL ) & 0x7fffUL );

	/* An unsafe example of a 32-bit random number generator. */
	/* Assuming rand() returns a 15-bit number. */
	uint32_t ulResult =
		( ( ( ( uint32_t ) rand() ) & 0x7fffuL )       ) |
		( ( ( ( uint32_t ) rand() ) & 0x7fffuL ) << 15 ) |
		( ( ( ( uint32_t ) rand() ) & 0x0003uL ) << 30 );
	return (UBaseType_t) ulResult;
}

/* TODO:
 * Supply a random number to FreeRTOS+TCP stack.
 * THIS IS ONLY A DUMMY IMPLEMENTATION THAT RETURNS A PSEUDO RANDOM NUMBER
 * SO IS NOT INTENDED FOR USE IN PRODUCTION SYSTEMS.
 */
BaseType_t xApplicationGetRandomNumber(uint32_t* pulNumber)
{
	*(pulNumber) = uxRand();
	return pdTRUE;
}
/*-----------------------------------------------------------*/

//BaseType_t xApplicationGetRandomNumber( uint32_t *pulValue )
//{
////HAL_StatusTypeDef xResult;
//    BaseType_t xReturn = pdPASS /*pdFAIL*/;
////uint32_t ulValue;
////
////	xResult = HAL_RNG_GenerateRandomNumber( &hrng, &ulValue );
////	if( xResult == HAL_OK )
////	{
////		xReturn = pdPASS;
////		*pulValue = ulValue;
////	}
////	else
////	{
////		xReturn = pdFAIL;
////	}
//    *pulValue = 1234;
//	return xReturn;
//}
/*-----------------------------------------------------------*/

/* TODO
* Callback that provides the inputs necessary to generate a randomized TCP
* Initial Sequence Number per RFC 6528.  In this case just a psuedo random
* number is used so THIS IS NOT RECOMMENDED FOR PRODUCTION SYSTEMS.
*/
uint32_t ulApplicationGetNextSequenceNumber( uint32_t ulSourceAddress, uint16_t usSourcePort, uint32_t ulDestinationAddress, uint16_t usDestinationPort )
{
     ( void ) ulSourceAddress;
     ( void ) usSourcePort;
     ( void ) ulDestinationAddress;
     ( void ) usDestinationPort;

     return uxRand();
}
