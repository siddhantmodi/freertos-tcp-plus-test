/*
 * FreeRTOS Kernel V10.3.0
 * Copyright (C) 2020 MistyWest, Inc. or its affiliates.  All Rights Reserved.
 *
 * 1 tab == 4 spaces!
 */

/* Standard includes. */
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/* FreeRTOS+TCP includes. */
#include "FreeRTOS_IP.h"
#include "FreeRTOS_Sockets.h"

#include "TCPSendReceiveData.h"
#include "config.h"

#define TCP_RX_DATA_BUFFER_SIZE 512

/* Stores the stack size passed into vStartConfigServerTask() so it can be
reused when the server listening task creates tasks to handle connections. */
static uint16_t usUsedStackSize = 0;
static const char comparisonString[] = "TCP echo from LAPTOP-KDT4FODV";
static const char replyString[] =      "TCP reply from CORA-Z7-10";

/* Initialise all the private functions. */
static void prvConnectionListeningTask( void *pvParameters );
static void prvConfigServerConnectionInstance( void *pvParameters );
static void prvProcessRXConfigData( Socket_t xSocket, char* cRxedData, BaseType_t lBytesReceived );

/*-----------------------------------------------------------*/

void vStartConfigServerTask( uint16_t usStackSize, UBaseType_t uxPriority )
{
	/* Create the TCP echo server. */
	xTaskCreate( prvConnectionListeningTask, "ServerListener", usStackSize, NULL, uxPriority + 1, NULL );

	/* Remember the requested stack size so it can be re-used by the server
	listening task when it creates tasks to handle connections. */
	usUsedStackSize = usStackSize;
}
/*-----------------------------------------------------------*/

static void prvConnectionListeningTask( void *pvParameters )
{
	struct freertos_sockaddr xClient;
	Socket_t xListeningSocket, xConnectedSocket;
	socklen_t xSize = sizeof( xClient );
//	static const TickType_t xReceiveTimeOut = portMAX_DELAY;
	const BaseType_t xBacklog = 20; // TODO: Does this need to be 20? Probably not.

	/* Just to prevent compiler warnings. */
	( void ) pvParameters;

	xListeningSocket = vCreateTCPServerSocket(TCP_CONFIG_SERVER_PORT_NUM);
	FreeRTOS_listen( xListeningSocket, xBacklog );

	for( ;; )
	{
		/* Wait for a client to connect. */
		xConnectedSocket = FreeRTOS_accept( xListeningSocket, &xClient, &xSize );
		configASSERT( xConnectedSocket != FREERTOS_INVALID_SOCKET );

		/* Spawn a task to handle the connection. */
		xTaskCreate( prvConfigServerConnectionInstance, "EchoServer", usUsedStackSize, ( void * ) xConnectedSocket, tskIDLE_PRIORITY, NULL );
	}
}
/*-----------------------------------------------------------*/

static void prvConfigServerConnectionInstance( void *pvParameters )
{
	Socket_t xSocket;
	static char cRxedData[ TCP_RX_DATA_BUFFER_SIZE ];
	BaseType_t lBytesReceived;

    /* The socket has already been created and connected before
    being passed into this RTOS task using the RTOS task�s parameter. */
    xSocket = ( Socket_t ) pvParameters;

    for( ;; )
    {
        /* Receive another block of data into the cRxedData buffer. */
        lBytesReceived = FreeRTOS_recv( xSocket, &cRxedData, TCP_RX_DATA_BUFFER_SIZE, 0 );

        if( lBytesReceived > 0 )
        {
            /* Data was received, process it here. */
            prvProcessRXConfigData( xSocket, cRxedData, lBytesReceived );
        	int32_t totalBytesSent = 0, bytesSent = 0;
			int32_t totalBytes = sizeof(replyString)/sizeof(replyString[0]);
			while ((bytesSent >= 0) && (totalBytesSent < lBytesReceived))
			{
				bytesSent = FreeRTOS_send( xSocket, cRxedData, lBytesReceived - totalBytesSent, 0 );
				totalBytesSent += bytesSent;
			}
        }
        else if( lBytesReceived == 0 )
        {
            /* No data was received, but FreeRTOS_recv() did not return an error.
            Timeout? */
        }
        else
        {
            /* Error (maybe the connected socket already shut down the socket?).
            Attempt graceful shutdown. */
            FreeRTOS_shutdown( xSocket, FREERTOS_SHUT_RDWR );
            break;
        }
    }

    /* The RTOS task will get here if an error is received on a read.  Ensure the
    socket has shut down (indicated by FreeRTOS_recv() returning a FREERTOS_EINVAL
    error before closing the socket). */

    while( FreeRTOS_recv( xSocket, &cRxedData, TCP_RX_DATA_BUFFER_SIZE, 0 ) >= 0 )
    {
        /* Wait for shutdown to complete.  If a receive block time is used then
        this delay will not be necessary as FreeRTOS_recv() will place the RTOS task
        into the Blocked state anyway. */
        vTaskDelay( pdMS_TO_TICKS( 250 ) );

        /* Note � real applications should implement a timeout here, not just
        loop forever. */
    }

    /* Shutdown is complete and the socket can be safely closed. */
    FreeRTOS_closesocket( xSocket );

    /* Must not drop off the end of the RTOS task � delete the RTOS task. */
    vTaskDelete( NULL );
}
/*-----------------------------------------------------------*/

static void prvProcessRXConfigData( Socket_t xSocket, char* cRxedData, BaseType_t lBytesReceived )
{
	bool isMatch = 1;
	int32_t totalBytesSent = 0, bytesSent = 0;
	int32_t totalBytes = sizeof(replyString)/sizeof(replyString[0]);

    xil_printf("TCP Data Received: ");
    for (uint16_t idx = 0; idx < lBytesReceived; idx++ )
    {
    	xil_printf("%c", cRxedData[idx]);
    	if(cRxedData[idx] != comparisonString[idx])
    	{
    		isMatch = 0;
    	}
    }
    xil_printf("\r\n");

    if( isMatch == 1)
    {
    	xil_printf("DATA MATCH! Sending data back \r\n");
//    	while ((bytesSent >= 0) && (totalBytesSent < totalBytes))
//    	{
//    		bytesSent = FreeRTOS_send( xSocket, replyString, totalBytes - totalBytesSent, 0 );
//    		totalBytesSent += bytesSent;
//    	}
    }
}
/*-----------------------------------------------------------*/

//void sendConfigDataPacket( configInfo_t pConfigInfo )
//{
//	Socket_t xSocket;
//	struct freertos_sockaddr xDataServerAddress;
//	volatile uint32_t ulTxCount = 0UL;
//	BaseType_t xReceivedBytes, xReturned, xInstance;
//	BaseType_t lTransmitted, lStringLength;
//	char *pcTransmittedString, *pcReceivedString;
//	WinProperties_t xWinProps;
//	TickType_t xTimeOnEntering;
//
//	/* Fill in the buffer and window sizes that will be used by the socket. */
//	xWinProps.lTxBufSize = 6 * ipconfigTCP_MSS;
//	xWinProps.lTxWinSize = 3;
//	xWinProps.lRxBufSize = 6 * ipconfigTCP_MSS;
//	xWinProps.lRxWinSize = 3;
//
//	/* This task can be created a number of times.  Each instance is numbered
//	to enable each instance to use a different Rx and Tx buffer.  The number is
//	passed in as the task's parameter. */
//	xInstance = ( BaseType_t ) pvParameters;
//
//	/* Point to the buffers to be used by this instance of this task. */
//	pcTransmittedString = &( cTxBuffers[ xInstance ][ 0 ] );
//	pcReceivedString = &( cRxBuffers[ xInstance ][ 0 ] );
//
//	/* Data is sent to the server. The address of the server is
//	 * configured by the data stored in pConfigInfo. */
//	xEchoServerAddress.sin_port = FreeRTOS_htons( pConfigInfo.portNum );
//	xEchoServerAddress.sin_addr = FreeRTOS_inet_addr_quick( pConfigInfo.serverAddr0,
//															pConfigInfo.serverAddr1,
//															pConfigInfo.serverAddr2,
//															pConfigInfo.serverAddr3 );
//
//	for( ;; )
//	{
//		/* Create a TCP socket. */
//		xSocket = FreeRTOS_socket( FREERTOS_AF_INET, FREERTOS_SOCK_STREAM, FREERTOS_IPPROTO_TCP );
//		configASSERT( xSocket != FREERTOS_INVALID_SOCKET );
//
//		/* Set a time out so a missing reply does not cause the task to block
//		indefinitely. */
//		FreeRTOS_setsockopt( xSocket, 0, FREERTOS_SO_RCVTIMEO, &xReceiveTimeOut, sizeof( xReceiveTimeOut ) );
//		FreeRTOS_setsockopt( xSocket, 0, FREERTOS_SO_SNDTIMEO, &xSendTimeOut, sizeof( xSendTimeOut ) );
//
//		/* Set the window and buffer sizes. */
//		FreeRTOS_setsockopt( xSocket, 0, FREERTOS_SO_WIN_PROPERTIES, ( void * ) &xWinProps,	sizeof( xWinProps ) );
//
//		/* Connect to the data server. */
//		if( FreeRTOS_connect( xSocket, &xDataServerAddress, sizeof( xDataServerAddress ) ) == 0 )
//		{
//			/* Create the data container that is sent to the echo server. */
//			configResult_t
//		}
//}
/*-----------------------------------------------------------*/
