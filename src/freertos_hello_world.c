///*
//    Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
//    Copyright (c) 2012 - 2020 Xilinx, Inc. All Rights Reserved.
//	SPDX-License-Identifier: MIT
//
//
//    http://www.FreeRTOS.org
//    http://aws.amazon.com/freertos
//
//
//    1 tab == 4 spaces!
//*/
//
///* FreeRTOS includes. */
//#include "FreeRTOS.h"
//#include "task.h"
//#include "queue.h"
//#include "timers.h"
//
///* FreeRTOS+TCP includes. */
//#include "FreeRTOS_IP.h"
//#include "FreeRTOS_Sockets.h"
////#include "FreeRTOS_tcp_server.h"
//#include "FreeRTOS_DHCP.h"
//
///* Xilinx includes. */
//#include "xil_printf.h"
//#include "xparameters.h"
//
///* AXI GPIO Driver */
//#include "xgpio.h"
//
//#define TIMER_ID	1
//#define DELAY_10_SECONDS	10000UL
//#define DELAY_1_SECOND		1000UL
//#define TIMER_CHECK_THRESHOLD	9
//#define GPIO_CHANNEL_BUTTON     1
//#define GPIO_CHANNEL_LED        2
//#define mainHOST_NAME			"RTOSDemo"
//#define mainDEVICE_NICK_NAME	"Zynq"
///*-----------------------------------------------------------*/
//
///* The Tx and Rx tasks as described at the top of this file. */
//static void prvTxTask ( void *pvParameters );
//static void prvRxTask ( void *pvParameters );
//static void prvLEDTask( void *pvParameters );
//static void vTimerCallback( TimerHandle_t pxTimer );
///*-----------------------------------------------------------*/
//
///* Default MAC address configuration. */
//const uint8_t ucMACAddress[ 6 ] = { configMAC_ADDR0, configMAC_ADDR1, configMAC_ADDR2, configMAC_ADDR3, configMAC_ADDR4, configMAC_ADDR5 };
//
///* Handle of the task that runs the FTP and HTTP servers. */
//static TaskHandle_t xServerWorkTaskHandle = NULL;
//
///* The queue used by the Tx and Rx tasks, as described at the top of this
//file. */
//static TaskHandle_t xTxTask;
//static TaskHandle_t xRxTask;
//static TaskHandle_t xLEDTask;
//static QueueHandle_t xQueue = NULL;
//static TimerHandle_t xTimer = NULL;
//static XGpio gpio_0;
//static u32 button, led;
//char HWstring[15] = "Hello World";
//long RxtaskCntr = 0;
//
//int main( void )
//{
//	const TickType_t x10seconds = pdMS_TO_TICKS( DELAY_10_SECONDS );
//
//	xil_printf( "Hello from Freertos example main\r\n" );
//
//	XGpio_Initialize(&gpio_0, XPAR_AXI_GPIO_0_DEVICE_ID); // 0 is the device ID for GPIO 0
//	XGpio_SetDataDirection(&gpio_0, GPIO_CHANNEL_LED,    0x00000000); // set LED GPIO channel (channel 2) tristates to All Output
//	XGpio_SetDataDirection(&gpio_0, GPIO_CHANNEL_BUTTON, 0xFFFFFFFF); // set Button GPIO channel (channel 1) tristates to All Input
//
//	/* Create the two tasks.  The Tx task is given a lower priority than the
//	Rx task, so the Rx task will leave the Blocked state and pre-empt the Tx
//	task as soon as the Tx task places an item in the queue. */
//	xTaskCreate( 	prvTxTask, 					/* The function that implements the task. */
//					( const char * ) "Tx", 		/* Text name for the task, provided to assist debugging only. */
//					configMINIMAL_STACK_SIZE, 	/* The stack allocated to the task. */
//					NULL, 						/* The task parameter is not used, so set to NULL. */
//					tskIDLE_PRIORITY,			/* The task runs at the idle priority. */
//					&xTxTask );
//
//	xTaskCreate( prvRxTask,
//				 ( const char * ) "GB",
//				 configMINIMAL_STACK_SIZE,
//				 NULL,
//				 tskIDLE_PRIORITY + 1,
//				 &xRxTask );
//
//	xTaskCreate( prvLEDTask,
//				 ( const char * ) "LED",
//				 configMINIMAL_STACK_SIZE,
//				 NULL,
//				 tskIDLE_PRIORITY,
//				 &xLEDTask );
//
//	/* Create the queue used by the tasks.  The Rx task has a higher priority
//	than the Tx task, so will preempt the Tx task and remove values from the
//	queue as soon as the Tx task writes to the queue - therefore the queue can
//	never have more than one item in it. */
//	xQueue = xQueueCreate( 	1,						/* There is only one space in the queue. */
//							sizeof( HWstring ) );	/* Each space in the queue is large enough to hold a uint32_t. */
//
//	/* Check the queue was created. */
//	configASSERT( xQueue );
//
//	/* Create a timer with a timer expiry of 10 seconds. The timer would expire
//	 after 10 seconds and the timer call back would get called. In the timer call back
//	 checks are done to ensure that the tasks have been running properly till then.
//	 The tasks are deleted in the timer call back and a message is printed to convey that
//	 the example has run successfully.
//	 The timer expiry is set to 10 seconds and the timer set to not auto reload. */
//	xTimer = xTimerCreate( (const char *) "Timer",
//							x10seconds,
//							pdFALSE,
//							(void *) TIMER_ID,
//							vTimerCallback);
//	/* Check the timer was created. */
//	configASSERT( xTimer );
//
//	/* start the timer with a block time of 0 ticks. This means as soon
//	   as the schedule starts the timer will start running and will expire after
//	   10 seconds */
//	xTimerStart( xTimer, 0 );
//
//	/* Start the tasks and timer running. */
//	vTaskStartScheduler();
//
//	/* If all is well, the scheduler will now be running, and the following line
//	will never be reached.  If the following line does execute, then there was
//	insufficient FreeRTOS heap memory available for the idle and/or timer tasks
//	to be created.  See the memory management section on the FreeRTOS web site
//	for more details. */
//	for( ;; );
//}
//
//
///*-----------------------------------------------------------*/
//static void prvTxTask( void *pvParameters )
//{
//const TickType_t x1second = pdMS_TO_TICKS( DELAY_1_SECOND );
//
//	for( ;; )
//	{
//		/* Delay for 1 second. */
//		vTaskDelay( x1second );
//
//		/* Send the next value on the queue.  The queue should always be
//		empty at this point so a block time of 0 is used. */
//		xQueueSend( xQueue,			/* The queue being written to. */
//					HWstring, /* The address of the data being sent. */
//					0UL );			/* The block time. */
//	}
//}
//
///*-----------------------------------------------------------*/
//static void prvRxTask( void *pvParameters )
//{
//char Recdstring[15] = "";
//
//	for( ;; )
//	{
//		/* Block to wait for data arriving on the queue. */
//		xQueueReceive( 	xQueue,				/* The queue being read. */
//						Recdstring,	/* Data is read into this address. */
//						portMAX_DELAY );	/* Wait without a timeout for data. */
//
//		/* Print the received data. */
//		xil_printf( "Rx task received string from Tx task: %s\r\n", Recdstring );
//		RxtaskCntr++;
//	}
//}
//
///*-----------------------------------------------------------*/
//static void prvLEDTask( void *pvParameters )
//{
//    for( ;; )
//	{
//    	// TODO: Add delay and see what happens
//		button = XGpio_DiscreteRead(&gpio_0, GPIO_CHANNEL_BUTTON);
//
//		if (button != 0) // turn all LEDs on when any button is pressed
//			led = 0xFFFFFFFF;
//		else
//			led = 0x00000000;
//
//		XGpio_DiscreteWrite(&gpio_0, GPIO_CHANNEL_LED, led);
//
//		xil_printf("\rbutton state: %08x", button);
//	}
//}
//
///*-----------------------------------------------------------*/
//static void vTimerCallback( TimerHandle_t pxTimer )
//{
//	long lTimerId;
//	configASSERT( pxTimer );
//
//	lTimerId = ( long ) pvTimerGetTimerID( pxTimer );
//
//	if (lTimerId != TIMER_ID) {
//		xil_printf("FreeRTOS Hello World Example FAILED");
//	}
//
//	/* If the RxtaskCntr is updated every time the Rx task is called. The
//	 Rx task is called every time the Tx task sends a message. The Tx task
//	 sends a message every 1 second.
//	 The timer expires after 10 seconds. We expect the RxtaskCntr to at least
//	 have a value of 9 (TIMER_CHECK_THRESHOLD) when the timer expires. */
//	if (RxtaskCntr >= TIMER_CHECK_THRESHOLD) {
//		xil_printf("Successfully ran FreeRTOS Hello World Example");
//	} else {
//		xil_printf("FreeRTOS Hello World Example FAILED");
//	}
//
//	XGpio_DiscreteWrite(&gpio_0, GPIO_CHANNEL_LED, 0);
//	vTaskDelete( xRxTask );
//	vTaskDelete( xTxTask );
//	vTaskDelete( xLEDTask );
//}
//
///* Called by FreeRTOS+TCP when the network connects or disconnects.  Disconnect
//events are only received if implemented in the MAC driver. */
//void vApplicationIPNetworkEventHook( eIPCallbackEvent_t eNetworkEvent )
//{
//uint32_t ulIPAddress, ulNetMask, ulGatewayAddress, ulDNSServerAddress;
//char cBuffer[ 16 ];
//static BaseType_t xTasksAlreadyCreated = pdFALSE;
//
//	/* If the network has just come up...*/
//	if( eNetworkEvent == eNetworkUp )
//	{
//		/* Create the tasks that use the IP stack if they have not already been
//		created. */
//		if( xTasksAlreadyCreated == pdFALSE )
//		{
//			/* See the comments above the definitions of these pre-processor
//			macros at the top of this file for a description of the individual
//			demo tasks. */
//
//			/* Let the server work task now it can now create the servers. */
//			if( xServerWorkTaskHandle != NULL )
//			{
//				xTaskNotifyGive( xServerWorkTaskHandle );
//			}
//
//			/* Register example commands with the FreeRTOS+CLI command
//			interpreter via the UDP port specified by the
//			mainUDP_CLI_PORT_NUMBER constant. */
//			#if( mainCREATE_UDP_CLI_TASKS == 1 )
//			{
//				vRegisterSampleCLICommands();
//				vRegisterFileSystemCLICommands();
//				vRegisterTCPCLICommands();
//				vStartUDPCommandInterpreterTask( mainUDP_CLI_TASK_STACK_SIZE, mainUDP_CLI_PORT_NUMBER, mainUDP_CLI_TASK_PRIORITY );
//			}
//			#endif
//
//			#if( mainCREATE_TCP_ECHO_CLIENT_TASKS_SINGLE == 1 )
//			{
//				vStartTCPEchoClientTasks_SingleTasks( mainECHO_CLIENT_TASK_STACK_SIZE, mainECHO_CLIENT_TASK_PRIORITY );
//			}
//			#endif
//
//			#if( mainCREATE_SIMPLE_TCP_ECHO_SERVER == 1 )
//			{
//				vStartSimpleTCPServerTasks( mainECHO_SERVER_STACK_SIZE, mainECHO_SERVER_TASK_PRIORITY );
//			}
//			#endif
//
//			/* Start a new task to fetch logging lines and send them out. */
//			#if( mainCREATE_UDP_LOGGING_TASK == 1 )
//			{
//				vUDPLoggingTaskCreate();
//			}
//			#endif
//
//			xTasksAlreadyCreated = pdTRUE;
//		}
//
//		/* Print out the network configuration, which may have come from a DHCP
//		server. */
//		FreeRTOS_GetAddressConfiguration( &ulIPAddress, &ulNetMask, &ulGatewayAddress, &ulDNSServerAddress );
//		FreeRTOS_inet_ntoa( ulIPAddress, cBuffer );
//		FreeRTOS_printf( ( "IP Address: %s\r\n", cBuffer ) );
//
//		FreeRTOS_inet_ntoa( ulNetMask, cBuffer );
//		FreeRTOS_printf( ( "Subnet Mask: %s\r\n", cBuffer ) );
//
//		FreeRTOS_inet_ntoa( ulGatewayAddress, cBuffer );
//		FreeRTOS_printf( ( "Gateway Address: %s\r\n", cBuffer ) );
//
//		FreeRTOS_inet_ntoa( ulDNSServerAddress, cBuffer );
//		FreeRTOS_printf( ( "DNS Server Address: %s\r\n\r\n\r\n", cBuffer ) );
//	}
//}
///*-----------------------------------------------------------*/
//
//const char *pcApplicationHostnameHook( void )
//{
//	/* Assign the name "FreeRTOS" to this network node.  This function will be
//	called during the DHCP: the machine will be registered with an IP address
//	plus this name. */
//	return mainHOST_NAME;
//}
///*-----------------------------------------------------------*/
//
//BaseType_t xApplicationDNSQueryHook( const char *pcName )
//{
//BaseType_t xReturn;
//
//	/* Determine if a name lookup is for this node.  Two names are given
//	to this node: that returned by pcApplicationHostnameHook() and that set
//	by mainDEVICE_NICK_NAME. */
//	if( strcasecmp( pcName, pcApplicationHostnameHook() ) == 0 )
//	{
//		xReturn = pdPASS;
//	}
//	else if( strcasecmp( pcName, mainDEVICE_NICK_NAME ) == 0 )
//	{
//		xReturn = pdPASS;
//	}
//	else
//	{
//		xReturn = pdFAIL;
//	}
//
//	return xReturn;
//}
///*-----------------------------------------------------------*/
//
///* Called automatically when a reply to an outgoing ping is received. */
//void vApplicationPingReplyHook( ePingReplyStatus_t eStatus, uint16_t usIdentifier )
//{
//static const char *pcSuccess = "Ping reply received - ";
//static const char *pcInvalidChecksum = "Ping reply received with invalid checksum - ";
//static const char *pcInvalidData = "Ping reply received with invalid data - ";
//
//	switch( eStatus )
//	{
//		case eSuccess	:
//			FreeRTOS_printf( ( pcSuccess ) );
//			break;
//
//		case eInvalidChecksum :
//			FreeRTOS_printf( ( pcInvalidChecksum ) );
//			break;
//
//		case eInvalidData :
//			FreeRTOS_printf( ( pcInvalidData ) );
//			break;
//
//		default :
//			/* It is not possible to get here as all enums have their own
//			case. */
//			break;
//	}
//
//	FreeRTOS_debug_printf( ( "identifier %d\r\n", ( int ) usIdentifier ) );
//
//	/* Prevent compiler warnings in case FreeRTOS_debug_printf() is not defined. */
//	( void ) usIdentifier;
//}
//
///* TODO: Create a method to generate a random number specific to the Cortex A9. */
//BaseType_t xApplicationGetRandomNumber( uint32_t *pulValue )
//{
////HAL_StatusTypeDef xResult;
//    BaseType_t xReturn = 0;
////uint32_t ulValue;
////
////	xResult = HAL_RNG_GenerateRandomNumber( &hrng, &ulValue );
////	if( xResult == HAL_OK )
////	{
////		xReturn = pdPASS;
////		*pulValue = ulValue;
////	}
////	else
////	{
////		xReturn = pdFAIL;
////	}
//	return xReturn;
//}
//
///* TODO
//* Callback that provides the inputs necessary to generate a randomized TCP
//* Initial Sequence Number per RFC 6528.  In this case just a psuedo random
//* number is used so THIS IS NOT RECOMMENDED FOR PRODUCTION SYSTEMS.
//*/
//uint32_t ulApplicationGetNextSequenceNumber( uint32_t ulSourceAddress, uint16_t usSourcePort, uint32_t ulDestinationAddress, uint16_t usDestinationPort )
//{
//     ( void ) ulSourceAddress;
//     ( void ) usSourcePort;
//     ( void ) ulDestinationAddress;
//     ( void ) usDestinationPort;
//
//     return 0; // xApplicationGetRandomNumber();
//}
//
